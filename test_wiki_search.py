from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
import allure

@allure.title('Результатов поиска 20')
# @allure.severity(Severity.BLOCKER)
def test_wiki_search():
    driver = WebDriver(executable_path='D://selenium//chromedriver.exe') # создаем экземпляр драйвера
    with allure.step('Открываем страницу поиска'):
        driver.get('https://www.wikipedia.org') # говорим драйверу перейтипо ссылке

    with allure.step('Ищем Automation test'):
        search_input = driver.find_element_by_xpath('//input[@id="searchInput"]') # с помощью вебдрайвера находим элемент вот с таким локатором - поле ввода
        search_button = driver.find_element_by_xpath('//div[@class="search-container"]//button[@type="submit"]') # находим элемент с таким локатором - кнопка поиска
        search_input.send_keys('Automation test') # в поле ввода передаем этот текст
        search_button.click() # жмём на кнопку поиска

    def check_results_count(driver):
        inner_search_results = driver.find_elements_by_xpath('//li[@class="mw-search-result"]')
        return len(inner_search_results) == 20 # ждём пока количество результатов будет 20

    with allure.step ('Ожидаем что количество результатов теста будет 20'):
        WebDriverWait(driver, 5, 0.5).until(check_results_count, 'Количество результатовпоиска не равно 20') # это будет выполняться в течении 5 секунд с каждой 0,5 секундой до тех пор пока не вернется Тру

    with allure.step('Переходим по ссылке третьего результата'):
        search_results = driver.find_elements_by_xpath('//div[@class="mw-search-result-heading"]') #
        link = search_results[2].find_element_by_xpath('//*[@id="mw-content-text"]/div[4]/ul/li[3]/div[1]/a') #
        link.click() #

    driver.switch_to.window(driver.window_handles[0]) #
    with allure.step('Проверяем корректность Title страницы'):
        assert driver.title == 'DevOps — Википедия' #
